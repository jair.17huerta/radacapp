from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from django.template import Library
from .models import Partida, Subpartida, Concepto, Proveedor, SupervObra, Obra, Requisicion, Requisitador, OrdenSuministro, ListadoMat, MaestroMat, RequisicionConcep
from django.http import HttpResponseRedirect
from django.views import View
#IMPORTACIONES PARA GENERAR PDF'S
import os
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.contrib.staticfiles import finders
from .forms import ReConcepForm

# Create your views here.
class ReportListView(ListView):
    model = Obra


class ReportDetailView(DetailView):
    model = Obra

    def get_context_data(self, *args, **kwargs):
        # Asumiendo que el PK que estás pasando es del Modelo1
        pk = self.kwargs.get('pk')
        context = super(ReportDetailView, self).get_context_data(**kwargs)
        context['Partida'] = Partida.objects.get(pk=pk)
        context['ListadoMat'] = ListadoMat.objects.get(pk=pk)
        context['MaestroMat'] = MaestroMat.objects.get(pk=pk)
        return context

class ReportPdfView(View):
    def get(self, request, *args, **kwargs):
        template = get_template('partida/obra_detail.html')
        context = {'title': 'Mi primer PDF'}
        template.render(context)
        html = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="report.pdf"'
        pisa_status = pisa.CreatePDF(
            html, dest=response)
        # if error then show some funy view
        if pisa_status.err:
            return HttpResponse('We had some errors <pre>' + html + '</pre>')
        return response

        # return HttpResponse('Hello world')

    #LO COMENTADO SON PRUEBAS ANTERIORES
    # def get_calculo_report(self):
    #     return self.ListadoMat.cantidad*self.MaestroMat.p_unitario
    
    # model = Partida
    #name_partida = Partida
    # model2 = ListadoMat.objects.filter(cantidad=cantidad)
    # model3 = MaestroMat.objects.filter(p_unitario=p_unitario)
    # model2 = Partida.objects.filter(pk=pk)

class RequiConcepListView(ListView):
    model = RequisicionConcep

class RequiConcepCreateView(CreateView):
    model = RequisicionConcep
    form_class = ReConcepForm
    success_url = reverse_lazy('reports:requisiconcep')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Product.objects.filter(name__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.name
                    data.append(item)
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['requisicion_id'] = 'Creación de una Venta'
    #     context['entity'] = 'Ventas'
    #     context['list_url'] = self.success_url
    #     context['action'] = 'add'
    #     return context
    
class RequiConcepDetailView(DetailView):
    model = RequisicionConcep

    def get_context_data(self, *args, **kwargs):
        # Asumiendo que el PK que estás pasando es del Modelo1
        pk = self.kwargs.get('pk')
        context = super(RequiConcepDetailView, self).get_context_data(**kwargs)
        context['Concepto'] = Concepto.objects.get(pk=pk)
        return context