from django.contrib import admin
from .models import Partida, Subpartida, Concepto, Proveedor, SupervObra, Requisitador, Obra, Requisicion, ListadoMat, MaestroMat, OrdenSuministro, RequisicionConcep

class PartidaAdmin(admin.ModelAdmin):
    list_display = ('name','description')

class SubpartidaAdmin(admin.ModelAdmin):
    list_display = ('name', 'partida_id', 'created')

    def get_partida_id(self, obj):
        return obj.partida.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_partida_id.short_description = 'Partida'  #Renames column head

class ConceptoAdmin(admin.ModelAdmin):
    list_display = ('name', 'partida_id','subpartida_id', 'created')

    def get_partida_id(self, obj):
        return obj.partida.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_partida_id.short_description = 'Partida'  #Renames column head

    def get_subpartida_id(self, obj):
        return obj.subpartida.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_partida_id.short_description = 'Subartida'  #Renames column head

class ProveedorAdmin(admin.ModelAdmin):
    list_display = ('empresa', 'r_social', 'created')

class SupervObraAdmin(admin.ModelAdmin):
    list_display = ('name','tipo', 'created')

class RequisitadorAdmin(admin.ModelAdmin):
    list_display = ('name','apat', 'created')

class ObraAdmin(admin.ModelAdmin):
    list_display = ('name_obra', 'type_obra', 'created')

class RequisicionAdmin(admin.ModelAdmin):
    list_display = ('obra_id', 'requisitador_id', 'created')

    def get_obra_id(self, obj):
        return obj.obra.name_obra
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_obra_id.short_description = 'Obra'
     #Renames column head
    def get_requisitador_id(self, obj):
        return obj.requisitador.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_requisitador_id.short_description = 'Requisitador'  #Renames column head

class ListadoMatAdmin(admin.ModelAdmin):
    list_display = ('requisicion_id', 'material_id', 'created')

    def get_requisicion_id(self, obj):
        return obj.requisicion.obra_id
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_requisicion_id.short_description = 'Requisicion'
     #Renames column head
    def get_material_id(self, obj):
        return obj.requisitador.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_material_id.short_description = 'Concepto'  #Renames column head

class MaestroMatAdmin(admin.ModelAdmin):
    list_display = ('concepto_id', 'proveedor_id', 'created')
    
    def get_concepto_id(self, obj):
        return obj.concepto.obra_id
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_concepto_id.short_description = 'Concepto'
     #Renames column head
    def get_proveedor_id(self, obj):
        return obj.proveedor.empresa
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_proveedor_id.short_description = 'Proveedor'  #Renames column head

class OrdenSuministroAdmin(admin.ModelAdmin):
    list_display = ('requisicion_id', 'requisitador_id', 'created')

    def get_requisicion_id(self, obj):
        return obj.requisicion.obra_id
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_requisicion_id.short_description = 'Requisicion'

    def get_requisitador_id(self, obj):
        return obj.requisitador.name
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_requisitador_id.short_description = 'Requisitador'  #Renames column head

class RequisicionConcepAdmin(admin.ModelAdmin):
    list_display = ('requisicion_id', 'created')

    def get_requisicion_id(self, obj):
        return obj.requisicion.obra_id
    # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    get_requisicion_id.short_description = 'Requisicion'

    # def get_concepto_id(self, obj):
    #     return obj.concepto.name
    # # partida_id.admin_order_field  = 'partida_id'  #Allows column order sorting
    # get_concepto_id.short_description = 'Concepto'  #Renames column head

    # def get_concepto_id(self, obj):
    #     return ", ".join([c.name for c in obj.conceptos.name])
    # get_concepto_id.short_description = "Conceptos"

admin.site.register(Partida, PartidaAdmin)
admin.site.register(Subpartida, SubpartidaAdmin)
admin.site.register(Concepto, ConceptoAdmin)
admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(SupervObra, SupervObraAdmin)
admin.site.register(Requisitador, RequisitadorAdmin)
admin.site.register(Obra, ObraAdmin)
admin.site.register(Requisicion, RequisicionAdmin)
admin.site.register(ListadoMat, ListadoMatAdmin)
admin.site.register(MaestroMat, MaestroMatAdmin)
admin.site.register(OrdenSuministro, OrdenSuministroAdmin)
admin.site.register(RequisicionConcep, RequisicionConcepAdmin)