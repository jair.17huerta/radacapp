# Generated by Django 3.0.6 on 2021-04-12 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partida', '0003_obra_proveedor_requisitador_supervobra'),
    ]

    operations = [
        migrations.CreateModel(
            name='Requisicion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(verbose_name='Fecha')),
                ('estatus_id', models.SmallIntegerField(verbose_name='Estatus')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
                ('obra_id', models.ManyToManyField(to='partida.Obra', verbose_name='Obra')),
                ('requisitador_id', models.ManyToManyField(to='partida.Requisitador', verbose_name='Requisitador')),
            ],
            options={
                'verbose_name': 'Requisicion',
                'verbose_name_plural': 'Requisiciones',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='OrdenSuministro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(verbose_name='Fecha de suministro')),
                ('estatus_id', models.SmallIntegerField(verbose_name='Estatus')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
                ('requisicion_id', models.ManyToManyField(to='partida.Requisicion', verbose_name='Requisición')),
                ('requisitador_id', models.ManyToManyField(to='partida.Requisitador', verbose_name='Requisitador')),
            ],
            options={
                'verbose_name': 'OrdenesSuministros',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='MaestroMat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('p_unitario', models.CharField(max_length=200, verbose_name='Precio unitario')),
                ('fecha', models.DateField(verbose_name='Vigencia')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
                ('concepto_id', models.ManyToManyField(to='partida.Concepto', verbose_name='Concepto')),
                ('proveedor_id', models.ManyToManyField(to='partida.Proveedor', verbose_name='Proveedor')),
            ],
            options={
                'verbose_name': 'MaestroMat',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='ListadoMat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(verbose_name='Fecha')),
                ('cantidad', models.CharField(max_length=200, verbose_name='Cantidad')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')),
                ('material_id', models.ManyToManyField(to='partida.Concepto', verbose_name='Material')),
                ('requisicion_id', models.ManyToManyField(to='partida.Requisicion', verbose_name='Requisición')),
            ],
            options={
                'verbose_name': 'ListadoMat',
                'ordering': ['-created'],
            },
        ),
    ]
