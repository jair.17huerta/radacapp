from django.urls import path
from .views import ReportListView, ReportDetailView, ReportPdfView, RequiConcepListView, RequiConcepDetailView, RequiConcepCreateView

report_patterns = ([
    path('reports/', ReportListView.as_view(), name='reportlist'),
    path('<int:pk>/<slug:slug>/', ReportDetailView.as_view(), name='report'),
    path('invoice/pdf/<int:pk>/', ReportPdfView.as_view(), name='invoice'),
    # path('create/', PageCreate.as_view(), name="create"),
    # path('update/<int:pk>/', PageUpdate.as_view(), name="update"),
    # path('delete/<int:pk>/', PageDelete.as_view(), name="delete"),
    path('requisc/', RequiConcepListView.as_view(), name='requisiconcep'),
    path('<int:pk>/', RequiConcepDetailView.as_view(), name='requidetails'),
    path('create/', RequiConcepCreateView.as_view(), name="requicreate")
], 'reports')