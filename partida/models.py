from django.db import models

#Models Catalogo
class Partida(models.Model):
    name = models.CharField(verbose_name="Nombre", max_length=90)
    description = models.CharField(verbose_name="Descripcion", max_length=90)
    order = models.SmallIntegerField(verbose_name="Orden", default=0)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")
    
    class Meta:
        verbose_name = "partida"
        verbose_name_plural = "partidas"
        ordering = ['order','name']

    def __str__(self):
        return self.name


class Subpartida(models.Model):
    name = models.CharField(verbose_name="Nombre",max_length=90)
    description = models.CharField(verbose_name="Descripcion", max_length=90)
    partida_id = models.ForeignKey(Partida,verbose_name="Partida primaria", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta: 
        verbose_name = "subpartida"
        verbose_name_plural = "subpartidas"
        ordering = ['-created']

    def __str__(self):
        return self.name


class Concepto(models.Model):
    name = models.CharField(verbose_name="Nombre",max_length=90)
    description = models.CharField(verbose_name="Descripcion", max_length=90)
    partida_id = models.ForeignKey(Partida,verbose_name="Partida primaria", on_delete=models.CASCADE)
    subpartida_id = models.ForeignKey(Subpartida, verbose_name="Partida secundaria", on_delete=models.CASCADE)
    u_medida = models.CharField(verbose_name="Unidad de medida",max_length=10)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "concepto"
        verbose_name_plural = "conceptos"
        ordering = ['-created']
    
    def __str__(self):
        return self.name

class Proveedor(models.Model):
    empresa = models.CharField(verbose_name="Empresa", max_length=90)
    r_social = models.CharField(verbose_name="Razón social", max_length=90)
    rfc = models.CharField(verbose_name="RFC", max_length=14)
    regimen_fisc = models.CharField(verbose_name="Regimen fiscal", max_length=90)
    contacto = models.CharField(verbose_name="Contacto", max_length=90)
    email = models.CharField(verbose_name="Email",max_length=90)
    phone = models.CharField(verbose_name="Teléfono", max_length=90)
    codigo_p = models.CharField(verbose_name="Código postal", max_length=5)
    address = models.CharField(verbose_name="Dirección", max_length=200)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "proveedor"
        verbose_name_plural = "proveedores"
        ordering = ['-created']
    
    def __str__(self):
        return self.empresa

class SupervObra(models.Model):
    name = models.CharField(verbose_name="Nombre", max_length=200)
    apat = models.CharField(verbose_name="Apellido Paterno", max_length=200)
    amat = models.CharField(verbose_name="Apellido Materno", max_length=200)
    tipo = models.CharField(verbose_name="Tipo de supervisor", max_length=200)
    estatus_id = models.SmallIntegerField(verbose_name="Estatus", default=0)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Supervisor"
        verbose_name_plural = "Supervisoras"
        ordering = ['-created']
    
    def __str__(self):
        return self.name

class Requisitador(models.Model):
    name = models.CharField(verbose_name="Nombre", max_length=200)
    apat = models.CharField(verbose_name="Apellido Paterno", max_length=200)
    amat = models.CharField(verbose_name="Apellido Materno", max_length=200)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Requisitador"
        verbose_name_plural = "Requisitadores"
        ordering = ['-created']
    
    def __str__ (self):
        return self.name

#Models Procesos
class Obra(models.Model):
    name_obra = models.CharField(verbose_name="Nombre de obra", max_length=200)
    type_obra = models.CharField(verbose_name="Tipo de obra", max_length=200)
    supervisor_id = models.ForeignKey(SupervObra, verbose_name="Supervisor", on_delete=models.CASCADE)
    estatus_id = models.SmallIntegerField(verbose_name="Estatus", default=0)
    state = models.CharField(verbose_name="Estado", max_length=200)
    town = models.CharField(verbose_name="Municipio", max_length=200)
    address = models.CharField(verbose_name="Dirección", max_length=200)
    clave = models.CharField(verbose_name="Clave", max_length=200)
    f_inicio = models.DateField(verbose_name="Fecha de inicio")
    f_fin = models.DateField(verbose_name="Fecha de fin")
    monto_p = models.CharField(verbose_name="Monto de presupuesto", max_length=200)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Obra"
        verbose_name_plural = "Obras"
        ordering = ['-created']
    
    def __str__(self):
        return self.name_obra

class Requisicion(models.Model):
    obra_id = models.ForeignKey(Obra, verbose_name="Obra", on_delete=models.CASCADE)
    requisitador_id = models.ForeignKey(Requisitador, verbose_name="Requisitador", on_delete=models.CASCADE)
    fecha = models.DateField(verbose_name="Fecha")
    estatus_id = models.SmallIntegerField(default=0,verbose_name="Estatus")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Requisicion"
        verbose_name_plural = "Requisiciones"
        ordering = ['-created']
    
    def __str__(self):
        return "%s %s" % (self.obra_id, self.requisitador_id)

class ListadoMat(models.Model):
    requisicion_id = models.ForeignKey(Requisicion, verbose_name="Requisición", on_delete=models.CASCADE)
    material_id = models.ForeignKey(Concepto, verbose_name="Material", on_delete=models.CASCADE)
    fecha = models.DateField(verbose_name="Fecha")
    cantidad = models.IntegerField(verbose_name="Cantidad")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "ListadoMat"
        ordering = ['-created']
    
    def __str__(self):
        return "%s %s" % (self.requisicion_id, self.material_id)

class MaestroMat(models.Model):
    concepto_id = models.ForeignKey(Concepto, verbose_name="Concepto", on_delete=models.CASCADE)
    proveedor_id = models.ForeignKey(Proveedor, verbose_name="Proveedor", on_delete=models.CASCADE)
    p_unitario = models.IntegerField(verbose_name="Precio unitario")
    fecha = models.DateField(verbose_name="Vigencia")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "MaestroMat"
        ordering = ['-created']

    def __str__(self):
        return "%s %s" % (self.concepto_id, self.proveedor_id)

class OrdenSuministro(models.Model):
    requisicion_id = models.ForeignKey(Requisicion, verbose_name="Requisición", on_delete=models.CASCADE)
    requisitador_id = models.ForeignKey(Requisitador, verbose_name="Requisitador", on_delete=models.CASCADE)
    fecha = models.DateField(verbose_name="Fecha de suministro")
    estatus_id = models.SmallIntegerField(verbose_name="Estatus")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name ="OrdenesSuministros"
        ordering =['-created']
    
    def __str__(self):
        return "%s %s" % (self.requisicion_id, self.requisitador_id)

class RequisicionConcep(models.Model):
    requisicion_id = models.ForeignKey(Requisicion, verbose_name="Requisición", on_delete=models.CASCADE)
    concepto_id = models.ManyToManyField(Concepto, verbose_name="Conceptos")
    cantidad = models.CharField(verbose_name="cantidad de materiales", max_length=200)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name="Requisicion de Materiales"
        ordering=['-created']
    
    def __str__(self):
        return "%s %s" % (self.requisicion_id, self.concepto_id)