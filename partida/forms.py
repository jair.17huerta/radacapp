from django.forms import *
from .models import RequisicionConcep, Concepto, Requisicion

class ReConcepForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = RequisicionConcep
        fields = '__all__'
        widgets = {
        'requisicion_id' : Select(attrs={'class':
         'form-control select2',
         'style': 'width: 100%'
         }),
        'concepto_id': Select(attrs={'class':
         'form-control select2',
         'style': 'width: 100%'
         }),
         'cantidad': TextInput(attrs={'class':
         'form-control'})
        }
